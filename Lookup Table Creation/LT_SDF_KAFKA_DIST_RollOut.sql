CREATE FOREIGN TABLE DISCOUNTED_OFFERS_DTLS 
(OFFER_NAME VARCHAR(400) NOT NULL,
SDF_SID VARCHAR(15) NOT NULL,
OFFER_ID VARCHAR(50) NOT NULL) 
SERVER ODS_DB OPTIONS (SCHEMA 'APPS_SAS_ODS', TABLE 'DISCOUNTED_OFFERS_DTLS');


CREATE VIEW LT_SDF_KAFKA_DIST (sdf_sid,offer_id,offer_name,ctid) AS SELECT DISTINCT sdf_sid,offer_id,offer_name,sdf_sid FROM discounted_offers_dtls;

INSERT INTO LOOKUPTABLE_REGISTER ( LOOKUPTABLE_NAME , SOURCE, IS_FILE , LOAD , STATE , UPDATED , FLAGS , DESCRIPTION ) VALUES ( 'LT_SDF_KAFKA_DIST' ,  'LT_SDF_KAFKA_DIST' , 0 , 1 , 1 , null , 0 , 'LT_SDF_KAFKA_DIST') ; 

INSERT INTO LOOKUPSERVER_TABLES (lookupserver_name,lookuptable_name) VALUES ('in1_server','LT_SDF_KAFKA_DIST');
INSERT INTO LOOKUPTABLE_TYPE ( LOOKUPTABLE_NAME , COLUMN_NAME , COLUMN_ORDER , COLUMN_TYPE , COLUMN_MODE ) VALUES ( 'LT_SDF_KAFKA_DIST' , 'SDF_SID' , 1 , 0 , 0 );
INSERT INTO LOOKUPTABLE_TYPE ( LOOKUPTABLE_NAME , COLUMN_NAME , COLUMN_ORDER , COLUMN_TYPE , COLUMN_MODE ) VALUES ( 'LT_SDF_KAFKA_DIST' , 'OFFER_ID ' , 2, 9 , 0) ;
INSERT INTO LOOKUPTABLE_TYPE ( LOOKUPTABLE_NAME , COLUMN_NAME , COLUMN_ORDER , COLUMN_TYPE , COLUMN_MODE ) VALUES ( 'LT_SDF_KAFKA_DIST' , 'OFFER_NAME', 3 , 9 , 0) ;

INSERT INTO Lookup_Screens(CompanyID,GroupID,ScreenID,Title,TableName,DisplayOrder,Description) VALUES('stc', 'SDP', 'LT_SDF_KAFKA_DI', 'LT_SDF_KAFKA_DI', 'LT_SDF_KAFKA_DIST', 1 , 'Eligible offers for discount and push to Kafka distr.');

INSERT INTO Lookup_Fields(CompanyID,GroupID,ScreenID,ColumnName,Title,Width,Length,DisplayOrder,Description,Type,TypeAttributes,Changeability,ObligatoryType) VALUES('stc', 'SDP', 'LT_SDF_KAFKA_DI', 'OFFER_ID', 'Eligible_SID', 50, 50, 2, 'Eligible_SID', 'string', '', 'RW',NULL);
INSERT INTO Lookup_Fields(CompanyID,GroupID,ScreenID,ColumnName,Title,Width,Length,DisplayOrder,Description,Type,TypeAttributes,Changeability,ObligatoryType) VALUES('stc', 'SDP', 'LT_SDF_KAFKA_DI', 'OFFER_NAME', 'Description', 400, 400, 3, 'Description', 'string', '', 'RW',NULL);
INSERT INTO Lookup_Fields(CompanyID,GroupID,ScreenID,ColumnName,Title,Width,Length,DisplayOrder,Description,Type,TypeAttributes,Changeability,ObligatoryType) VALUES('stc', 'SDP', 'LT_SDF_KAFKA_DI', 'SDF_SID', 'Service_ID', 15, 15, 1, 'Service_ID', 'string', '', 'RW',NULL);

